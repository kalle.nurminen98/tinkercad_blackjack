# Tinkercad blackjack game

This is simplified version of blackjack game (one player against dealer, no split and no bet).

# Equiments

Equipments: 1k Ohm resistors, potentiometer, Arduino, LCD display, two push buttons

# Restrictions

There is small restriction, you can press the button maximum of six times. If your not pleased with the value after that, you need to start the program again.

![](./Circuit.png)

# Usage

Build similar circuit and add code to code section in tinkercad first. To start the game press left push button to get card to player and press the button to get more cards and when you are

pleased with your value of cards, press right push button to give dealer

cards. When you start the program and the LCD don’t have power, start the program again or

press potentiometer value again to max to get power to LCD display.

# Maintainers

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)
