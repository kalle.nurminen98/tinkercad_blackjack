/*
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)


*/

// iclude library
// C code
#include <LiquidCrystal.h>
LiquidCrystal lcd(12,11,5,4,3,2);

/*int total,Mtotal;
int point;*/
const byte button1=7;
const byte button2=8;
int playervalue=0;
int playervalue1=0;
int playervalue2=0;
int playervalue3=0;
int playervalue4=0;
int playervalue5=0;
int playervalue6=0;
int playervalue7=0;
int playervalue8=0;
int playervalue9=0;
int playervalue10=0;
int dealervalue=0;
int dealervalue1=0;
int dealervalue2=0;
int dealervalue3=0;
int dealervalue4=0;
int dealervalue5=0;
int dealervalue6=0;
int dealervalue7=0;
int dealervalue8=0;
int dealervalue9=0;
int dealervalue10=0;
int dealerpoints=0;
int playerpoints=0;
byte buttonState1=0;
byte buttonState2=0;
byte ppressed=0;
byte dpressed=0;
byte lastButtonState1=HIGH;
byte lastButtonState2=HIGH;
bool ppressed1 = false;
bool ppressed2 = false;
bool ppressed3 = false;
bool ppressed4 = false;
bool ppressed5 = false;
bool dpressed1 = false;
bool dpressed2 = false;
bool dpressed3 = false;
bool dpressed4 = false;
bool dpressed5 = false;
char Dealer[6] ={'0','0','0','0','0','0'};
char Player[6]={'0','0','0','0','0','0'};



char intTOchar(int a)
{
  switch(a)
  {
    case 1:return 'A';
    case 2:return '2';
    case 3:return '3';
    case 4:return '4';
    case 5:return '5';
    case 6:return '6';
    case 7:return '7';
    case 8:return '8';
    case 9:return '9';
    case 10:return 'T';
    case 11:return 'J';
    case 12:return 'Q';
    case 13:return 'K';
  }
}

int charToint(char c)
{
  switch(c)
  {
    case '0':return 0;
    case 'A':return 1;
    case '2':return 2;
    case '3':return 3;
    case '4':return 4;
    case '5':return 5;
    case '6':return 6;
    case '7':return 7;
    case '8':return 8;
    case '9':return 9;
    case 'T':return 10;
    case 'J':return 10;
    case 'Q':return 10;
    case 'K':return 10; 
  }
}

char deal(void){
  int deal = ((random(100)%13) +1);
  return intTOchar(deal);
}

void blackjack(void){
  if((playervalue2==11)&&((Player[1]=='A')||(Player[2]=='A'))){
    playervalue2=21;
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Blackjack");
    lcd.setCursor(0, 1);
    lcd.print("You won");
    
  }
  if((dealervalue2==11)&& ((Dealer[1]=='A')||(Dealer[2]=='A'))){
    dealervalue2=21;
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Blackjack");
    lcd.setCursor(0, 1);
    lcd.print("Dealer won");
    
  }
 }

void bust(void){
  if(playervalue4 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("You lost");
  }
  if(playervalue6 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("You lost");
  }
  if(playervalue8 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("You lost");
  }
  if(playervalue10 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("You lost");
  }
  if(dealervalue4 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer lost");
  }
  if(dealervalue6 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer lost");
  }
  if(dealervalue8 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer lost");
  }
  if(dealervalue10 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer lost");
  }
}




void stand(void){
  
  if(dealervalue2 > 21){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer lost");
  }
  
  
  else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 > playervalue10)
         && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 < playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 == playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 > playervalue8)
         && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 < playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 == playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 > playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 < playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 == playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 > playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 < playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 == playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 < playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue10 >= 17) && (dealervalue10 <= 21) && (dealervalue10 == playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 > playervalue10)
         && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 < playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 == playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 > playervalue8)
         && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 < playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 == playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 > playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 < playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 == playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 > playervalue4)
         && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 < playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 == playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 < playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue8 >= 17) && (dealervalue8 <= 21) && (dealervalue8 == playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 > playervalue10)
         && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 < playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 == playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 > playervalue8)
         && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 < playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 == playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 > playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 < playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 == playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 > playervalue4)
         && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 < playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 == playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 < playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue6 >= 17) && (dealervalue6 <= 21) && (dealervalue6 == playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 > playervalue10)
         && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 < playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 == playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
   else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 > playervalue8)
          && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 < playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 == playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 > playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 < playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 == playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
 }
  
  else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 > playervalue4)
         && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 < playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 == playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 < playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue4 >= 17) && (dealervalue4 <= 21) && (dealervalue4 == playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 > playervalue10)
         && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 < playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 == playervalue10)
        && (playervalue10 > playervalue8)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 > playervalue8)
         && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 < playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 == playervalue8)
        && (playervalue8 > playervalue6)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 > playervalue6)
        && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 < playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 == playervalue6)
         && (playervalue6 > playervalue4)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 > playervalue4)
         && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 < playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 == playervalue4)
        && (playervalue4 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }
  
  else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 > playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dealer wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 < playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Player wins");
  }
 else if((dealervalue2 >= 17) && (dealervalue2 <= 21) && (dealervalue2 == playervalue2)){
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Its tie");
  }  
}
    


void setup()
{
  lcd.begin(16,2);
  lcd.clear();
  lcd.setCursor(0, 0);
  Serial.begin(9600);
  pinMode(7,INPUT_PULLUP);
  pinMode(8,INPUT_PULLUP);
  randomSeed(analogRead(A0));
  
}

void loop()
{
    checkButton1();
    checkButton2();
    if (ppressed == 1 && ppressed1 == true){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue: ");
      
      Player[1] = deal();
      playervalue = charToint(Player[1]);
   
      lcd.setCursor(13, 0);
      lcd.print(playervalue);
      lcd.setCursor(13, 1);
      lcd.print(playervalue);
      ppressed1 = false;
    }
    else if (ppressed == 2 && ppressed2 == true){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard2: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue2: ");
      Player[2] = deal();
      playervalue1 = charToint(Player[2]);
      playervalue2 = playervalue1 + playervalue;
      lcd.setCursor(13, 0);
      lcd.print(playervalue1);
      lcd.setCursor(13, 1);
      lcd.print(playervalue2);
      blackjack();
      stand();
      ppressed2 = false;
      
      }
      
    
  else if (ppressed == 3 && ppressed3 == true){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard3: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue3: ");
      Player[3] = deal();
      playervalue3 = charToint(Player[3]);
      playervalue4 = playervalue3 + playervalue2;
      lcd.setCursor(13, 0);
      lcd.print(playervalue3);
      lcd.setCursor(13, 1);
      lcd.print(playervalue4);
      bust();
      stand();
      ppressed3 = false;
    
    }
  else if (ppressed == 4 && ppressed4 == true){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard4: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue4: ");
      Player[4] = deal();
      playervalue5 = charToint(Player[4]);
      playervalue6 = playervalue4 + playervalue5;
      lcd.setCursor(13, 0);
      lcd.print(playervalue5);
      lcd.setCursor(13, 1);
      lcd.print(playervalue6);
      bust();
      stand();
      ppressed4 = false;
    
    }
  else if (ppressed == 5 && ppressed5 ==true){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard5: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue5: ");
      Player[5] = deal();
      playervalue7 = charToint(Player[5]);
      playervalue8 = playervalue6 + playervalue7;
      lcd.setCursor(13, 0);
      lcd.print(playervalue7);
      lcd.setCursor(13, 1);
      lcd.print(playervalue8);
      bust();  
      stand();
      ppressed5 = false;
    
    }
  else if (ppressed == 6){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("UserCard6: ");
   
      lcd.setCursor(0, 1);
      lcd.print("UserValue6: ");
      Player[6] = deal();
      playervalue9 = charToint(Player[5]);
      playervalue10 = playervalue8 + playervalue9;
      lcd.setCursor(13, 0);
      lcd.print(playervalue9);
      lcd.setCursor(13, 1);
      lcd.print(playervalue10);
      bust();  
      stand();
      ppressed = 0;
    
    }
   if (dpressed == 1 && dpressed1 == true){
   lcd.clear();
   lcd.setCursor(0, 0);
   lcd.print("DealerCard: ");
   lcd.setCursor(0, 1);
   lcd.print("DealerValue: ");
   
   Dealer[1] = deal();
   dealervalue = charToint(Dealer[1]);
   
   lcd.setCursor(13, 0);
   lcd.print(dealervalue);
   lcd.setCursor(13, 1);
   lcd.print(dealervalue);
   dpressed1 = false;
  }
  else if(dpressed == 2 && dpressed2 == true){
    lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DealerCard2: ");
   
      lcd.setCursor(0, 1);
      lcd.print("DealerValue2: ");
      Dealer[2] = deal();
      dealervalue1 = charToint(Dealer[2]);
      dealervalue2 = dealervalue1 + dealervalue;
      lcd.setCursor(13, 0);
      lcd.print(dealervalue1);
      lcd.setCursor(13, 1);
      lcd.print(dealervalue2);
      blackjack();
      stand();
      dpressed2 = false;
     }
  else if(dpressed == 3 && dpressed3 == true){
    lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DealerCard3: ");
   
      lcd.setCursor(0, 1);
      lcd.print("DealerValue3: ");
      Dealer[3] = deal();
      dealervalue3 = charToint(Dealer[3]);
      dealervalue4 = dealervalue2 + dealervalue3;
      lcd.setCursor(13, 0);
      lcd.print(dealervalue3);
      lcd.setCursor(13, 1);
      lcd.print(dealervalue4);
      bust();
      stand();
      dpressed3 = false;
     }
  else if(dpressed == 4 && dpressed4 == true){
    lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DealerCard4: ");
   
      lcd.setCursor(0, 1);
      lcd.print("DealerValue4: ");
      Dealer[4] = deal();
      dealervalue5 = charToint(Dealer[4]);
      dealervalue6 = dealervalue4 + dealervalue5;
      lcd.setCursor(13, 0);
      lcd.print(dealervalue5);
      lcd.setCursor(13, 1);
      lcd.print(dealervalue6);
      bust();
      stand();
      dpressed4 = false;
     }
  else if(dpressed == 5 && dpressed5 == true){
    lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DealerCard5: ");
   
      lcd.setCursor(0, 1);
      lcd.print("DealerValue5: ");
      Dealer[5] = deal();
      dealervalue7 = charToint(Dealer[5]);
      dealervalue8 = dealervalue6 + dealervalue7;
      lcd.setCursor(13, 0);
      lcd.print(dealervalue7);
      lcd.setCursor(13, 1);
      lcd.print(dealervalue8);
      bust();
      stand();
      dpressed5 = false;
     }
  else if(dpressed == 6){
    lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DealerCard6: ");
   
      lcd.setCursor(0, 1);
      lcd.print("DealerValue6: ");
      Dealer[6] = deal();
      dealervalue9 = charToint(Dealer[6]);
      dealervalue10 = dealervalue8 + dealervalue9;
      lcd.setCursor(13, 0);
      lcd.print(dealervalue9);
      lcd.setCursor(13, 1);
      lcd.print(dealervalue10);
      bust();
      stand();
      dpressed = 0;
     }
    }
 
void checkButton1()
{
  buttonState1 = digitalRead(button1);

  if (lastButtonState1 != buttonState1)
  {
    lastButtonState1 = buttonState1;

    
    if (buttonState1 == HIGH)
    {
      ppressed++;
      if (ppressed == 1)
      {
        
        ppressed1 = true;
      }
       if (ppressed == 2)
      {
        
        ppressed2 = true;
      }
      if (ppressed == 3)
      {
        
        ppressed3 = true;
      }
      if (ppressed == 4)
      {
        
        ppressed4 = true;
      }
      if (ppressed == 5)
      {
        
        ppressed5 = true;
      }

    }
  }
}

void checkButton2()
{
  buttonState2 = digitalRead(button2);

  if (lastButtonState2 != buttonState2)
  {
    lastButtonState2 = buttonState2;

    
    if (buttonState2 == HIGH)
    {
      dpressed++;
      if (dpressed == 1)
      {
        
        dpressed1 = true;
      }
       if (dpressed == 2)
      {
        
        dpressed2 = true;
      }
      if (dpressed == 3)
      {
        
        dpressed3 = true;
      }
      if (dpressed == 4)
      {
        
        dpressed4 = true;
      }
      if (dpressed == 5)
      {
        
        dpressed5 = true;
      }

    }
  }
}